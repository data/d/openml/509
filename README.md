# OpenML dataset: places

https://www.openml.org/d/509

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This dataset is taken from the Places Rated Almanac, by Richard
Boyer and David Savageau, copyrighted and published by Rand McNally.
This book order (SBN) number is 0-528-88008-X, and it retails for
$14.95 .  The data are reproduced by kind permission of the
publisher, and with the request that the copyright notice of Rand
McNally, and the names of the authors appear in any paper or
presentation using these data.

The nine rating criteria used by Places Rated Almanac are:
Climate & Terrain
Housing
Health Care & Environment
Crime
Transportation
Education
The Arts
Recreation
Economics

For all but two of the above criteria, the higher the score, the
better.  For Housing and Crime, the lower the score the better.

The scores are computed using the following component statistics for
each criterion (see the Places Rated Almanac for details):

Climate & Terrain: very hot and very cold months, seasonal
temperature variation, heating- and cooling-degree days, freezing
days, zero-degree days, ninety-degree days.

Housing: utility bills, property taxes, mortgage payments.

Health Care & Environment: per capita physicians, teaching hospitals,
medical schools, cardiac rehabilitation centers, comprehensive cancer
treatment centers, hospices, insurance/hospitalization costs index,
flouridation of drinking water, air pollution.

Crime: violent crime rate, property crime rate.

Transportation: daily commute, public transportation, Interstate
highways, air service, passenger rail service.

Education: pupil/teacher ratio in the public K-12 system, effort
index in K-12, accademic options in higher education.

The Arts: museums, fine arts and public radio stations, public
television stations, universities offering a degree or degrees in the
arts, symphony orchestras, theatres, opera companies, dance
companies, public libraries.

Recreation: good restaurants, public golf courses, certified lanes
for tenpin bowling, movie theatres, zoos, aquariums, family theme
parks, sanctioned automobile race tracks, pari-mutuel betting
attractions, major- and minor- league professional sports teams, NCAA
Division I football and basketball teams, miles of ocean or Great
Lakes coastline, inland water, national forests, national parks, or
national wildlife refuges, Consolidated Metropolitan Statistical Area
access.

Economics: average household income adjusted for taxes and living
costs, income growth, job growth.


The data are recorded in two ASCII files, PLACES.DAT, and
PLACES.KEY .  The first file contains 329 observations, 9 columns
plus an index column.  The index stands for the location.  PLACES.KEY
gives the index in the first column and the associated name of the
place in the second column.  All data analysis can be done with
numeric variables and the index, as read in from PLACES.DAT .
Alternatively, the numerical key can be replaced by the alphabetic
name, as given by PLACES.KEY .


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: 9

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/509) of an [OpenML dataset](https://www.openml.org/d/509). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/509/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/509/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/509/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

